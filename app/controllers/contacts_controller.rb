class ContactsController < ApplicationController


  def index
    if params[:view].nil?
      contacts = Contact.all
    else
      contacts = Contact.where(:id => params[:view])
    end

    render :json => contacts
  end

  def show
    contact = Contact.find_by(:id => params[:id])
    if contact
      render :json => contact, :status => :ok
    else
      render :json => {:success => false}, :status => :unprocessable_entity
    end

  end

  def create
    contact = Contact.new(create_params)
    if contact.save
      render :json => contact
    else
      render :json => contact.errors.full_messages
    end
  end

  private

  def create_params
    #params.select{|k,v| [:title].member?(k) }
    {:title => params[:title], :order => params[:order], :done => params[:done]}
  end
end